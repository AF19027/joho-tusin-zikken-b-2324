import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class sampleGUI{
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane receivedorder;
    private JButton sushiButton;
    private JButton sobaButton;
    private JButton odenButton;
    private JTextPane allprice;
    private JButton checkout;

    int all = 0;

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food +"!  It will be served as soon as possible.");
            String currentText = receivedorder.getText();
            receivedorder.setText(currentText + food +" "+ price +"yen"+"\n");
            all += price;
            allprice.setText(all +"yen");
        }
    }

    public sampleGUI(){
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",500);
                            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tempura_photo.jpg")
                ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",800);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen_photo.jpg")
        ));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",300);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Udon_photo.jpg")
        ));

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",900);
            }
        });
        sushiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Sushi_photo.jpg")
        ));

        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",400);
            }
        });
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Soba_photo.jpg")
        ));

        odenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oden",600);
            }
        });
        odenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Oden_photo.jpg")
        ));
        allprice.setText(all +"yen");

        checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int okaikei = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?","Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (okaikei == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you.The total price is " + all +" yen.");
                    all = 0;
                    allprice.setText(all +"yen");
                    receivedorder.setText("");
                }


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("sampleGUI");
        frame.setContentPane(new sampleGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

